<?php

/**
 * @file
 * Contains \Drupal\archive\Form\ArchiveAdminSettings.
 */

namespace Drupal\archive\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class ArchiveAdminSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'archive_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('archive.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['archive.settings'];
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $types = node_type_get_types();
    $final_types = [];
    foreach ($types as $key => $value) {
      $final_types[$key] = $value->name;
    }

    // @FIXME
    // Could not extract the default value because it is either indeterminate, or
    // not scalar. You'll need to provide a default value in
    // config/install/archive.settings.yml and config/schema/archive.schema.yml.
    $form['archive_type_filters'] = [
      '#type' => 'checkboxes',
      '#title' => t('Content types available in archive'),
      '#default_value' => \Drupal::config('archive.settings')->get('archive_type_filters'),
      '#options' => $final_types,
      '#description' => t('Posts of these types will be displayed in the archive.'),
    ];

    return parent::buildForm($form, $form_state);
  }

}
