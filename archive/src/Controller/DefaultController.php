<?php /**
 * @file
 * Contains \Drupal\archive\Controller\DefaultController.
 */

namespace Drupal\archive\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Default controller for the archive module.
 */
class DefaultController extends ControllerBase {

  public function archive_page($type = 'all', $year = 0, $month = 0, $day = 0) {

    // Make sure all values are secure.
    $day = (int) $day;
    $year = (int) $year;
    $month = (int) $month;
    if ($month < 0 || $month > 12) {
      // Ensure that we have a proper array index later.
      $month = 0;
    }
    if (!_archive_validate_type($type)) {
      $type = 'all';
    }

    // @FIXME
    // drupal_set_title() has been removed. There are now a few ways to set the title
    // dynamically, depending on the situation.
    // 
    // 
    // @see https://www.drupal.org/node/2067859
    // drupal_set_title(theme('archive_page_title', array('type' => $type, 'year' => $year, 'month' => $month, 'day' => $day)));


    $date = _archive_date($type, $year, $month, $day);
    $query = _archive_query($type, $date);
    // @FIXME
    // // @FIXME
    // // This looks like another module's variable. You'll need to rewrite this call
    // // to ensure that it uses the correct configuration object.
    // $nodes = variable_get('default_nodes_main', 10);

    $result = $query->extend('PagerDefault')->limit($nodes)->execute();
    $nodes = $query->countQuery()->execute()->fetchField();

    // @FIXME
    // The Assets API has totally changed. CSS, JavaScript, and libraries are now
    // attached directly to render arrays using the #attached property.
    // 
    // 
    // @see https://www.drupal.org/node/2169605
    // @see https://www.drupal.org/node/2408597
    // drupal_add_css(drupal_get_path('module', 'archive') .'/archive.css');

    // @FIXME
    // theme() has been renamed to _theme() and should NEVER be called directly.
    // Calling _theme() directly can alter the expected output and potentially
    // introduce security issues (see https://www.drupal.org/node/2195739). You
    // should use renderable arrays instead.
    // 
    // 
    // @see https://www.drupal.org/node/2195739
    // $output = theme('archive_navigation', array('type' => $type, 'date' => $date));


    if (!$nodes && $type == 'all') {
      $output .= t('No content found.');
      return $output;
    }

    $found_rows = FALSE;
    $node_date = 0;
    foreach ($result as $o) {
      $node = \Drupal::entityManager()->getStorage('node')->load($o->nid);
      $node_created = $node->created + $date->tz;
      // Determine which separators are needed
      $separators = [
        'year' => 0,
        'month' => 0,
        'day' => 0,
      ];
      if (!$year) {
        $created_year = format_date($node_created, 'custom', 'Y');
        $last_year = format_date($node_date, 'custom', 'Y');
        if ($created_year != $last_year) {
          $separators['year'] = 1;
        }
      }
      // Print month separaters
      if (!$month) {
        $created_month = format_date($node_created, 'custom', 'n');
        $last_month = format_date($node_date, 'custom', 'n');
        if ($created_month != $last_month) {
          $separators['month'] = 1;
        }
      }
      // Print day separaters
      if (!$day) {
        $created_day = format_date($node_created, 'custom', 'j');
        $last_day = format_date($node_date, 'custom', 'j');
        if ($created_day != $last_day) {
          $separators['day'] = 1;
        }
      }
      // @FIXME
      // theme() has been renamed to _theme() and should NEVER be called directly.
      // Calling _theme() directly can alter the expected output and potentially
      // introduce security issues (see https://www.drupal.org/node/2195739). You
      // should use renderable arrays instead.
      // 
      // 
      // @see https://www.drupal.org/node/2195739
      // $output .= theme('archive_separator', array('date_created' => $node_created, 'separators' => $separators));

      $output .= \Drupal::service("renderer")->render(node_view($node, 'teaser'));

      $found_rows = TRUE;
      $node_date = $node->created + $date->tz;
    }
    if ($found_rows) {
      // @FIXME
// theme() has been renamed to _theme() and should NEVER be called directly.
// Calling _theme() directly can alter the expected output and potentially
// introduce security issues (see https://www.drupal.org/node/2195739). You
// should use renderable arrays instead.
// 
// 
// @see https://www.drupal.org/node/2195739
// $output .= theme('pager');

    }
      // Handle URLs that are incorrectly typed and try to parse info out of them
    else {
      if ($date->days[$date->day]) {
        drupal_goto(_archive_url($type, $date->year, $date->month, $date->day));
      }
      elseif ($date->months[$date->month]) {
        drupal_goto(_archive_url($type, $date->year, $date->month));
      }
      elseif ($date->years[$date->year]) {
        drupal_goto(_archive_url($type, $date->year));
      }
      else {
        drupal_goto(_archive_url($type));
      }
    }

    return $output;
  }

}
